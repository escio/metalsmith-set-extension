'use strict';

const Path = require('path');

// Change the extension of all files to the specified one:
module.exports = (newExtension) => {
	return (files, metalsmith, done) => {
		for (const filename in files) {
			if (files.hasOwnProperty(filename)) {
				const extension = Path.extname(filename);
				const dirname = Path.dirname(filename);
				const basename = Path.basename(filename, extension);
				const newFilename = Path.join(dirname, basename + newExtension);

				files[newFilename] = files[filename];
				delete files[filename];
			}
		}

		done();
	};
};
