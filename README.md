# @escio/metalsmith-set-extension

> Metalsmith plugin setting the extension of processed files

## Installation

```
$ npm install @escio/metalsmith-set-extension
```

## Usage

The plugin has a single parameter, the extension you wish to set on the processed files. The existing extension is overwritten.

```javascript
const metalsmith = require('metalsmith');
const setExtension = require('@escio/metalsmith-set-extension');

metalsmith(
    __dirname
).use(
    // Set the extension of all processed files to .html
    setExtension('.html')
).build((err) => {
    if (err) {
        throw err;
    }
});
```

## License

ISC © Escio AS
